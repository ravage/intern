module Intern
  class TeamRequest < Request

    ENDPOINTS =  {
      teams: '/Teams'
    }

    def teams
      endpoint = build_endpoint(__method__)
      result = get(endpoint)
      result.map { |v| Team.new(v) }
    end

    def endpoints
      ENDPOINTS
    end
  end
end
