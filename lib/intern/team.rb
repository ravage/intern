module Intern
  class Team
    include Virtus.value_object
    include Normalize

    attribute :id, String
    attribute :name, String
    attribute :captain, String
    attribute :players, Array[Summoner]

    def transform()
      {
        id:       'TeamId',
        name:     'TeamName',
        captain:  'CaptainNick'
      }
    end
  end
end
