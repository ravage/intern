module Intern
  class ServiceUnavailable < StandardError
    def message
      '503: Service Unavailable'
    end
  end
end
