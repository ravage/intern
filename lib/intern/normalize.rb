module Intern
  module Normalize
    def initialize(attributes)
      normalized = {}

      transform.each do |k, v|
        attributes[k] = attributes[v]
        attributes.delete(v)
      end

      attributes.each { |k, v| normalized[k.downcase] = v }

      @raw = normalized

      super(normalized)
    end

    def transform(attributes = {})
      attributes
    end

    def raw
      @raw
    end
  end
end
