module Intern
  class Request

    def initialize
      url = 'http://legendsapi.ismai.pt'

      @conn = Faraday.new(url: url) do |faraday|
        faraday.request  :url_encoded
        faraday.adapter  :excon
      end
    end

    def endpoint_prefix
      '/api'
    end

    def build_endpoint(method, placeholder: nil, values: nil)
      if placeholder
        endpoint_prefix << endpoints[method].sub(placeholder, values)
      else
        endpoint_prefix << endpoints[method]
      end
    end

    def escape(value)
      CGI.escape value.downcase.gsub(/\s/, '')
    end

    def get(endpoint, query = nil)
      response = @conn.get(endpoint)

      return '[]' if response.status == 404

      raise ServiceUnavailable if response.status == 503

      MultiJson.load(response.body)
    end

  end
end
