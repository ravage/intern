module Intern
  class Summoner
    include Virtus.value_object
    include Normalize

    attribute :id, String
    attribute :name, String
    attribute :nick, String

    def transform
      {
        nick: 'PlayerNick',
        name: 'PlayerName'
      }
    end
  end
end
